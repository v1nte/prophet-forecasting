# Prophet forecasting project

[![pipeline status](https://gitlab.com/v1nte/prophet-forecasting/badges/main/pipeline.svg)](https://gitlab.com/v1nte/prophet-forecasting/-/commits/main)

This project aims to predict marine environmental data of the "Seno de Reloncaví".

## Setup

You have four option to run this project

### Option 1(Recomended): Docker Compose

```bash
git clone https://gitlab.com/v1nte/prophet-forecasting
docker-compose up
```

### Option 2: Docker (no need to clone)

```bash
docker run --name forecaster -v ./results:/app/results -v ./images:/app/images -v ./warnings:/app/warnings registry.gitlab.com/v1nte/prophet-forecasting:main
```

### Option 3: Python with virtualenv

```bash
git clone https://gitlab.com/v1nte/prophet-forecasting
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python main.py
```
### Option 4: Python

```bash
git clone https://gitlab.com/v1nte/prophet-forecasting
pip install -r requirements.txt
python main.py
```
