from tqdm import tqdm
from time import sleep
from src.train import train_and_save
from src.utils import create_directories, get_ftp_files, parse_ftp_files, check_preds_for_warnings
from src.config import vars_list_dict
from schedule import every, repeat, run_pending, run_all


@repeat(every(3).hours)
def main():

    create_directories()
    get_ftp_files()
    parse_ftp_files()

    for var in tqdm(vars_list_dict):
        train_and_save(var, plot_components=True)
        check_preds_for_warnings(var)


if __name__ == '__main__':
    run_all()
    while True:
        run_pending()