import os
import scipy.io
import pandas as pd
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from os import getenv
from dotenv import load_dotenv
from ftplib import FTP



def create_directories():

    directories = (
        'images/predicts',
        'images/components',
        'results/',
        'data/',
        'warnings/'
    )

    for directory in directories:
        if not os.path.exists(directory):
            os.makedirs(directory)
            print(f'Directory {directory} created')
        else:
            print(f'Directory {directory} already exists')


def save_plot_to_local_system(var_name: str):
    mat_file = scipy.io.loadmat("mat_files/atmos.mat")

    var_serie = mat_file[var_name]

    serie = var_serie['Serie']

    data = serie[0][0][0][0][1].flatten()
    dates = serie[0][0][0][0][0].flatten()

    parsed_dates = [mat_to_date(x) for x in dates]

    fig = plt.figure(figsize=(19,9))
    plt.xlabel('fecha')
    plt.ylabel('Valor')
    plt.title(var_name)
    plt.plot(parsed_dates, data)
    fig.savefig(f'images/{var_name}.png', dpi=100, bbox_inches='tight')


def mat_to_date(matlab_date: int):
    ref_date = datetime(1, 1, 1)

    result = ref_date + timedelta(days=matlab_date-2) + relativedelta(years=-1)

    formated_date = result.strftime('%Y-%m-%d %H:%M:%S')

    return formated_date


def get_ftp_files():
    load_dotenv()
    print("FTP: Extracting files")
    server_files = ['CASCO CR6_atmosphericData.dat', 'CASCO CR6_sonde.dat']
    local_files = ['data/atmos.dat', 'data/water.dat']

    with FTP(os.getenv("FTP_IP")) as ftp:
        ftp.login(
            os.getenv("FTP_USER"),
            os.getenv("FTP_PASS")
        )

        for server_files, filename in zip(server_files, local_files):
            with open(filename, 'wb') as local_file:
                ftp.retrbinary('RETR ' + server_files, local_file.write)
    print("FTP: Done extracting files")


def parse_ftp_files():
    print("CSV: Parsing FTP Files")
    deadline = pd.to_datetime('2023-09-01')

    df_atmos = pd.read_csv('data/atmos.dat', skiprows = [0, 2, 3], usecols=[0,2,3,4,5,6,7,8], na_values='NAN')
    df_atmos['TIMESTAMP'] = pd.to_datetime(df_atmos['TIMESTAMP'], format='%Y-%m-%d %H:%M:%S')
    df_atmos = df_atmos[df_atmos['TIMESTAMP'] >= deadline]

    df_atmos.to_csv('data/atmos.csv', index = False)

    df_water = pd.read_csv('data/water.dat', skiprows = [0, 2, 3], usecols=[0,3,4,6,8,9], na_values='NAN')
    df_water['TIMESTAMP'] = pd.to_datetime(df_water['TIMESTAMP'], format='%Y-%m-%d %H:%M:%S')
    df_water = df_water[df_water['TIMESTAMP'] >= deadline]

    df_water.to_csv('data/water.csv', index = False)
    print("CSV: Done parsing FTP Files")

    os.remove('data/atmos.dat')
    os.remove('data/water.dat')

    init_date = '2023-09-12'
    end_date = '2023-09-14'

    indixes_to_delete = df_water.loc[(df_water['TIMESTAMP'] >= init_date) & (df_water['TIMESTAMP'] <= end_date)].index
    df_water = df_water.drop(indixes_to_delete)

    df_water = df_water.dropna()
    df_water.to_csv('data/water.csv', index = False)
    _remove_outlayers()
    

def split_csv_by_column(input_file: str, target_column: str, output_file: str):
    df = pd.read_csv(input_file)

    if target_column not in df.columns:
        print(f'ERROR!! Column "{target_column}" not found in "{input_file}"')

    new_df = df[['TIMESTAMP', target_column]]
    new_df.columns = ['ds', 'y']

    return new_df

def _remove_outlayers():
    atmos_indexes_to_remove = [ '2023-09-05 02:30:00', '2023-09-10 02:10:00']
    df = pd.read_csv('data/atmos.csv')

    df = df[~df['TIMESTAMP'].isin(atmos_indexes_to_remove)]
    df.to_csv('data/atmos.csv')


def check_preds_for_warnings(var: dict):

    csv_file = f'results/result_{var["type"]}_{var["name"]}.csv'
    df = pd.read_csv(csv_file)

    new_df = df[df['yhat'] > var['max']].copy()

    new_df['warning_type'] = 'greater_than'

    # new_df = new_df.append( df[df['yhat'] < var['min']].copy(), ignore_index=True)
    new_df = pd.concat([new_df, df[df['yhat'] < var['min']].copy()], ignore_index=True)

    new_df.loc[new_df['yhat'] < var['min'], 'warning_type'] = 'less_than'

    for index, row in new_df.iterrows():
        print(f'{row["ds"]}: {row["warning_type"]}')
    
    new_df.to_csv(f'warnings/{var["type"]}_{var["name"]}.csv', index=False)
