import scipy.io
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from prophet import Prophet

from src.utils import mat_to_date, split_csv_by_column
from src.config import vars_list_dict


def train_and_save(
        var: dict,
        plot_components=False,
        plot_results=True,
    ):

    if var['type'] == 'atmos':
        csv_file = 'data/atmos.csv'

    if var['type'] == 'water':
        csv_file = 'data/water.csv'

    updated_values = split_csv_by_column(
        csv_file,
        var['ftp_name'],
        f'data/{var["name"]}'
    )

    mat_file = scipy.io.loadmat(f'mat_files/{var["type"]}.mat')

    var_serie = mat_file[var['name']]

    serie = var_serie['Serie']

    values = serie[0][0][0][0][1].flatten()
    dates = serie[0][0][0][0][0].flatten()

    parsed_dates = [mat_to_date(x) for x in dates]
    new_dates = np.array(parsed_dates)
    df = pd.DataFrame({'ds': new_dates, 'y': values})

    df = pd.concat([df, updated_values], ignore_index=True)

    m = Prophet(
        changepoint_prior_scale=0.001,
        scaling='minmax', 
        # seasonality_mode='multiplicative'
    )

    m.fit(df)
    future = m.make_future_dataframe(periods=365*2, freq='D', include_history=False)

    forecast = m.predict(future)

    output_csv_name = f'results/result_{var["type"]}_{var["name"]}.csv'
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].to_csv(output_csv_name, index=False)

    if plot_results:
        fig1 = m.plot(forecast, figsize=(21,9))
        fig1.savefig(f'images/predicts/predict_{var["type"]}_{var["name"]}.png')

    if plot_components:
        fig2 = m.plot_components(forecast, figsize=(21,9))
        fig2.savefig(f'images/components/components_{var["type"]}_{var["name"]}.png')
