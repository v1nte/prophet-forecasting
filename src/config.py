variables = (
    ('atmos', 'VelViento_ms', 'avgWindSpeed', 0, 20),
    ('atmos', 'DirViento_deg', 'avgWindDir', 0, 360),
    ('atmos', 'RafVelViento_ms', 'gustWindSpeed', 0, 30),
    ('atmos', 'RafDirViento_deg', 'gustWindDir', 0, 360),
    ('atmos', 'PresionAire_mbar', 'maximetPressure', 990, 1030),
    ('atmos', 'TempAire_C', 'maximetTemperature', 0, 20),
    ('atmos', 'Humedad_porc', 'maximetHumidity', 70, 90),
    ('water', 'Temp_C', 'amlTemperature', 0, 30),
    ('water', 'Sal_psu', 'amlSalinity', 10, 35),
    ('water', 'DO_uM', 'amlDO', 100, 500),
    ('water', 'Chla_ugl', 'amlChlorphyll', 0, 40),
    ('water', 'Presion_m', 'amlPressure', 0.6, 1.2),
)

vars_list_dict = [ {
    'type': var[0],
    'name': var[1],
    'ftp_name': var[2],
    'min': var[3],
    'max': var[4]
    } for var in variables ]